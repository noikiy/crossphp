# 模板的使用

根据使用的模板引擎选择支持的语法,这里以CrossPHP框架的原生视图为例.

### 一. 使用默认视图控制器中的方法.

1. 生成连接

		$this->link("controller:action", array('key'=>'value'));
	
	> 生成后的url中的连接由模块配置文件init.php中的url中type和dot控制  

2. 生成加密连接
	
		$this->slink("controller:action", array('key'=>'value'));

	唯一不同的是`array('key'=>'value')` 部分是加密的	
	>在控制器中调用 `$this->sparams()` 来还原加密前的参数
	
3. 包含其他模板文件

		$this->tpl("page/p1");
	>引入page目录下的p1.tpl.php文件
	

### 二. 在对应的视图控制器中的扩展

如

    private function threadList( $data = array() )
    {
        foreach($data as $d)
        {
            ?>
			<li>
            	<a href="<?php echo $this->link("read", array('id'=>1)) ?>">标题</a>
			</li>
            <?php
        }
    }

在模板文件中用 `$this->threadList($data)` 调用.
>重复使用的,公共的的tpl可以放在app视图控制器自定义的基类中,保持模板代码的整洁