# 模型系统简介

	├─modules modules文件夹
	│   ├─admin
	│   ├─space
	│   ├─user
	│   ├─blog
	│   ├─article
	│   └─common

模型系统是所有app功能模块共同的数据来源,一个modules支持多个数据来源.目前默认支持以下的数据库系统.

- PDO驱动的MySQL
- Redis,
- Memcache,
- CouchBase,
- MongoDB

> 不支持不同类型数据库之间切换